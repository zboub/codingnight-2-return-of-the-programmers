package fr.gengame.main;

import java.awt.event.MouseEvent;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import fr.gengame.character.Personnage;
import fr.gengame.ennemies.Boss;
import fr.gengame.ennemies.Ennemies;
import fr.gengame.ennemies.Sbires;
import fr.gengame.menus.GOMenu;
import fr.gengame.menus.PauseMenu;
import fr.gengame.util.Collisions;
import fr.gengame.util.Projectile;
import fr.gengame.util.RayTracing;
import fr.gengame.util.Seg;
import fr.gengame.util.Vec;

public class WorldGenGame extends BasicGameState {

	public static int ID = 1;

	public static Personnage perso;
	public static ArrayList<Sbires> ennemiesTab = new ArrayList<Sbires>();
	public static ArrayList<Sbires> ennemiesInit = new ArrayList<Sbires>();
	public static Sbires ennemie1;
	public static Sbires ennemie2;
	public static Boss boss;
	private GameContainer container;
	static public StateBasedGame game;
	private RayTracing ray = new RayTracing();
	private Vec mouse;
	private Image background;
	ArrayList<Seg> obstacles = new ArrayList<Seg>();
	public static ArrayList<Projectile> projectiles = new ArrayList<Projectile>();
	int timerReset = 0;
	int reset = 360;
	int round;

	public static Integer[] scoreList = new Integer[] { 0, 0, 0, 0, 0 };

	Seg seg;

	// ======================================================================================
	// CONSTANTS DECLARATION
	// ======================================================================================

	static public int heightW = 600, widthW = 800;

	// ======================================================================================
	// VARIABLES DECLARATION
	// ======================================================================================

	static public boolean[] buttons = { false, false, false, false };

	// ======================================================================================
	// CONSTRUCTOR(S)
	// ======================================================================================

	// ======================================================================================
	// INIT FUNCTION
	// ======================================================================================

	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		this.container = container;
		container.setShowFPS(false);
		container.setTargetFrameRate(60);
		perso = new Personnage(new Seg(new Vec(400, 550), new Vec(0, 100)),1);
		new Sbires(new Seg(new Vec(0, 0), new Vec(0, 100)), 1,32);
		new Sbires(new Seg(new Vec(779, 0), new Vec(0, 100)), 1,32);
		new Sbires(new Seg(new Vec(0, 300), new Vec(0, 100)), 1,32);
		new Sbires(new Seg(new Vec(779, 300), new Vec(0, 100)), 1,32);
		boss = new Boss(new Seg(new Vec(255, 60), new Vec(0, 100)), 150, 231,290);
		mouse = new Vec(0, 0);
		this.game = game;
		background = new Image("Images/background.png");

		obstacles.add(new Seg(new Vec(0, 60), new Vec(0, 600)));//gauche
		obstacles.add(new Seg(new Vec(600, 300), new Vec(120, -150)));
		obstacles.add(new Seg(new Vec(200, 300), new Vec(-120, -150)));//coin
		obstacles.add(new Seg(new Vec(0, 60), new Vec(800, 0)));//haut
		obstacles.add(new Seg(new Vec(0, 600), new Vec(800, 0)));//bas
		obstacles.add(new Seg(new Vec(800, 60), new Vec(0, 600)));//droite
		//obstacles.add(new Seg(new Vec(300, 400), new Vec(200, 0)));//plat


	}

	public void reset(){
		perso.character.org = (new Vec(400, 550));
		boss.skin=boss.skinFort;
		round++;
		
		for(int i = 0; i < ennemiesInit.size();i++){
			if(!ennemiesTab.contains(ennemiesInit.get(i)))
				ennemiesTab.add(ennemiesInit.get(i));
		}
		for(int i = 0; i < ennemiesTab.size();i++){
			ennemiesTab.get(i).character.org.x = ennemiesTab.get(i).posIX;
			ennemiesTab.get(i).character.org.y = ennemiesTab.get(i).posIY;
			ennemiesTab.get(i).speed=new Vec(0.2+0.035*round,0.2+0.035*round);
		}


	}

	// ======================================================================================
	// RENDER FUNCTION
	// ======================================================================================

	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		g.drawImage(background, 0, 0);
		perso.render(g, mouse);

		seg = new Seg(perso.getPosAndDir().org, Vec.sub(mouse,
				perso.getPosAndDir().org));

		g.setColor(Color.green);
		for (int i = 0; i < obstacles.size(); i++)
			g.drawLine((float) obstacles.get(i).org.x,
					(float) obstacles.get(i).org.y, (float) obstacles.get(i)
					.getDest().x, (float) obstacles.get(i).getDest().y);

		g.setColor(Color.red);
		ArrayList<Vec> points = ray.rayTraceBounces(seg, obstacles, 3);
		for (int i = 0; i < points.size() - 1; i++) {
			g.drawLine((float) points.get(i).x, (float) points.get(i).y,
					(float) points.get(i + 1).x, (float) points.get(i + 1).y);
		}

		boss.render(g);

		for(int i = 0; i < projectiles.size();i++){
			projectiles.get(i).render(container,g);
		}
		for(int i = 0; i < ennemiesTab.size();i++){
			ennemiesTab.get(i).render(g);
		}
		
		g.setColor(Color.white);
		g.drawString("Boss: "+boss.getVie(), 380, 27);

	}

	// ===================================================================================s===
	// UPDATE FUNCTION
	// ======================================================================================

	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		try{
			perso.update(delta);
			boss.update(1);
			for(int i = 0; i < projectiles.size();i++){
				projectiles.get(i).update(container, delta);
				for(int j = 0; j< ennemiesTab.size();j++){
					if(Collisions.collisionCircleCircle(projectiles.get(i), ennemiesTab.get(j))){
						try{
							ennemiesTab.remove(j);
							if(ennemiesTab.size()==0){
								boss.skin=boss.skinFaible;
								boss.vieFaible = boss.getVie();
							}
						}catch(IndexOutOfBoundsException e){}
					}
				}

				if(Collisions.collisionCircleRect(projectiles.get(i),boss)){
					if(ennemiesTab.size()==0){
						boss.loseLife();
					}
					projectiles.remove(i);
				}

			}
			for(int i = 0;i<ennemiesTab.size();i++){
				ennemiesTab.get(i).update(delta);
				if(Collisions.collisionCircleCircle(perso, ennemiesTab.get(i))){
					reset();
					for(int j = 0; j < ennemiesTab.size();j++){
						ennemiesTab.get(j).speed=new Vec(0.2,0.2);
					}
					round = 0;
					boss.setVie(150);
					game.enterState(GOMenu.ID);
				}
			}
			if(Collisions.collisionCircleRect(perso, boss)){
				reset();
				for(int j = 0; j < ennemiesTab.size();j++){
					ennemiesTab.get(j).speed=new Vec(0.2,0.2);
				}
				round = 0;
				boss.setVie(150);
				game.enterState(GOMenu.ID);
			}
			if(boss.getVie()<=boss.vieFaible-20){
				timerReset = 0;
				reset();
				reset();
				boss.vieFaible = boss.getVie();
			}
		}catch(IndexOutOfBoundsException e){}
	}

	// ======================================================================================
	// KEYBOARD INPUTS
	// ======================================================================================

	public void keyReleased(int key, char c) {
		switch (key) {
		case Input.KEY_Z:
			buttons[0] = false;
			if (perso.getDirectionY() == -1) {
				if (buttons[1] == false)
					perso.setDirectionY(0);
				else
					perso.setDirectionY(1);
			}
			break;
		case Input.KEY_S:
			buttons[1] = false;
			if (perso.getDirectionY() == 1) {
				if (buttons[0] == false)
					perso.setDirectionY(0);
				else
					perso.setDirectionY(-1);
			}
			break;
		case Input.KEY_Q:
			buttons[2] = false;
			if (perso.getDirectionX() == -1) {
				if (buttons[3] == false)
					perso.setDirectionX(0);
				else
					perso.setDirectionX(1);
			}
			break;
		case Input.KEY_D:
			buttons[3] = false;
			if (perso.getDirectionX() == 1) {
				if (buttons[2] == false)
					perso.setDirectionX(0);
				else
					perso.setDirectionX(-1);
			}
			break;
		case Input.KEY_LSHIFT:
			break;
		}
	}

	public void keyPressed(int key, char c) {
		switch (key) {
		case Input.KEY_Z:
			buttons[0] = true;
			perso.setDirectionY(-1);
			break;
		case Input.KEY_S:
			buttons[1] = true;
			perso.setDirectionY(1);
			break;
		case Input.KEY_Q:
			buttons[2] = true;
			perso.setDirectionX(-1);
			break;
		case Input.KEY_D:
			buttons[3] = true;
			perso.setDirectionX(1);
			break;
		case Input.KEY_P:
			game.enterState(PauseMenu.ID);
			break;
		case Input.KEY_ESCAPE:
			game.enterState(PauseMenu.ID);
			break;
		}
	}
	public void mousePressed(int button,int x, int y){
		if(button == Input.MOUSE_LEFT_BUTTON)
			projectiles.add(new Projectile(seg, 1, ray.rayTraceBounces(seg, obstacles, 3)));
	}

	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		this.mouse.x = newx;
		this.mouse.y = newy;
	}

	public int getID() {
		return ID;
	}
}