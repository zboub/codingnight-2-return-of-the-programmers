package fr.gengame.main;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.StateBasedGame;

import fr.gengame.main.WorldGenGame;
import fr.gengame.menus.*;

public class Game extends StateBasedGame {
	

	public static void main(String[] args) throws SlickException {
		new AppGameContainer(new Game(), 800, 600, false).start();
	}

	public Game() {
		super("GeneralGame");
	}



	@Override
	public void initStatesList(GameContainer container) throws SlickException {

		//addState(new "different states of the game"());
		
		addState(new WelcomeMenu());
		addState(new MainMenu());
		addState(new WorldGenGame());
		addState(new GOMenu());
		addState(new PauseMenu());
		addState(new ConfirmMenu());
		addState(new CreditsMenu());


	}

}
 	