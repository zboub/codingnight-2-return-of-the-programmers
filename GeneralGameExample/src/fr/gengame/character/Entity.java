package fr.gengame.character;

import org.newdawn.slick.Image;

import fr.gengame.util.Seg;
import fr.gengame.util.Vec;

public abstract class Entity {
	
	public Seg character;
	public Vec speed;
	protected int vie;
	protected int directionX;
	protected int directionY;
	public Image skin;
	public double posIX;
	public double posIY;
	
	public Seg getCharacter() {
		return character;
	}

	public void setCharacter(Seg character) {
		this.character = character;
	}

	public Vec getSpeed() {
		return speed;
	}

	public void setSpeed(Vec speed) {
		this.speed = speed;
	}

	public int getVie() {
		return vie;
	}

	public void setVie(int vie) {
		this.vie = vie;
	}

	public int getDirectionX() {
		return directionX;
	}

	public void setDirectionX(int directionX) {
		this.directionX = directionX;
	}

	public int getDirectionY() {
		return directionY;
	}

	public void setDirectionY(int directionY) {
		this.directionY = directionY;
	}


	public Entity(Seg character, int vie) {
		super();
		this.character = character;
		this.vie = vie;
		this.speed = new Vec(0.33,0.33);
		posIX = character.org.x;
		posIY = character.org.y;
	}
	
	public void move(int delta){
		character.org=Vec.sum(character.org,new Vec(speed.x*delta*directionX,speed.y*delta*directionY) );
	}
	
	public void loseLife(){
		vie--;
		if(vie <= 0){
			die();
		}
	}
	
	public void die(){
		
	}

	public Seg getPosAndDir() {
		return character;
	}
	
}
