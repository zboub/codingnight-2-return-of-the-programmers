package fr.gengame.character;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import fr.gengame.util.Circle;
import fr.gengame.util.Seg;
import fr.gengame.util.Vec;

public class Personnage extends Entity implements Circle {

	private float angle;

	public Personnage(Seg character, int vie) {
		super(character, vie);
		directionX = 0;
		directionY = 0;
		try {
			this.skin = new Image("Images/character.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public double getX() {
		// TODO Auto-generated method stub
		return this.getCharacter().getOrigin().x;
	}

	@Override
	public double getY() {
		// TODO Auto-generated method stub
		return this.getCharacter().getOrigin().y;
	}

	@Override
	public int getRadius() {
		// TODO Auto-generated method stub
		return 32;
	}

	public void update(int delta) {
		move(delta);
		if (character.org.x > 800 - 32) {
			character.org.x = 800 - 32;
		}
		if (character.org.x < 32) {
			character.org.x = 32;
		}
		if (character.org.y > 600 - 32) {
			character.org.y = 600 - 32;
		}
		if (character.org.y < 92) {
			character.org.y = 92;
		}
	}

	// ---------------------------------------------------
	// RENDER FUNCTION
	// ---------------------------------------------------

	public void render(Graphics g, Vec mouse) {
		angle = (float) (Math.asin((mouse.y - character.org.y)
				/ Vec.sub(mouse, character.org).length()) * 180 / Math.PI);
		if (mouse.x < character.org.x) {
			skin.setRotation((float) (180 - angle));
		} else {
			skin.setRotation(angle);
		}
		g.drawImage(skin, (float) (character.org.x - 43),
				(float) (character.org.y - 32));
	}

}
