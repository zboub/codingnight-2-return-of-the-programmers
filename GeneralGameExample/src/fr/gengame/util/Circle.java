package fr.gengame.util;

public interface Circle {

	double getX();

	double getY();

	int getRadius();

}
