package fr.gengame.util;

public class Vec {
	
	public double x, y;
	
	public Vec(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public double length(){
		return Math.sqrt(x*x + y*y);
	}
	
	public double lengthSquared(){
		return x*x + y*y;
	}
	
	public Vec getVecWithNorm(double length)
	{
		return mul(length/length());
	}
	
	public Vec mul(double f)
	{
		return new Vec(x*f, y*f);
	}
	
	public static double scal(Vec v1, Vec v2){
		return v1.x*v2.x + v1.y*v2.y;
	}
	
	public static Vec sum(Vec v1, Vec v2){
		return new Vec(v1.x+v2.x, v1.y+v2.y);
	}
	
	public Vec proj(Vec AM)
	{
		double f = scal(this, AM)/lengthSquared();
		return mul(f);
	}
	
	public Vec normalize(){
		return mul(1/length());
	}
	
	public static boolean isColin(Vec v1, Vec v2){
		if(v1.y == 0)
			return v2.y == 0;
		
		if(v2.y == 0)
			return false;
		
		if(v1.x == 0)
			return v2.x == 0;
		
		if(v2.x == 0)
			return false;
		
		return (v1.y/v2.y == v1.x/v2.x);
	}
	
	public static Vec sub(Vec v1, Vec v2)
	{
		return new Vec(v1.x-v2.x, v1.y-v2.y);
	}
	
	public static double dist(Vec A, Vec B)
	{
		return sub(B, A).length();
	}
}
