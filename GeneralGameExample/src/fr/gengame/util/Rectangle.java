package fr.gengame.util;

public interface Rectangle {

	int getY();

	int getX();

	int getWidth();

	int getHight();

	
}
