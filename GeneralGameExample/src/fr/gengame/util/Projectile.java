package fr.gengame.util;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import fr.gengame.character.Entity;
import fr.gengame.main.WorldGenGame;

public class Projectile extends Entity implements Circle{
	
	static int idTotal = 0;
	public int id;
	private Vec nxtPoint;
	private int pointActuel = 1;
	private ArrayList<Vec> points;
	private double speedP = 0.8;

	//===================================================================
	//CONSTRUCTEURS
	//===================================================================
	
	public Projectile(Seg character, int vie,ArrayList<Vec> pts) {
		super(character, vie);
		id=idTotal;
		idTotal++;
		points = pts;
		nextPoint();
		setDir();
		this.character.org.x = WorldGenGame.perso.character.org.x;
		this.character.org.y = WorldGenGame.perso.character.org.y;
		for(int i = 0;i< points.size();i++){
		}
	}
	
	//===================================================================
	//RENDER-UPDATE
	//===================================================================
	

	public void update(GameContainer container, int delta){
		if(Math.abs(nxtPoint.x-character.org.x) <= 5 && Math.abs(nxtPoint.y-character.org.y) <= 5){
			nextPoint();
			setDir();
		}
		move(delta);
	}
	
	public void nextPoint(){
		try{
		nxtPoint = points.get(pointActuel);
		}catch(IndexOutOfBoundsException e){
			WorldGenGame.projectiles.remove(this);
		}
		pointActuel++;
	}
	public void setDir(){
		speed.x = Math.abs(((nxtPoint.x-character.org.x)*speedP)/Vec.sub(nxtPoint, character.org).length());
		speed.y = Math.abs(((nxtPoint.y-character.org.y)*speedP)/Vec.sub(nxtPoint, character.org).length());
		
		if(nxtPoint.x-character.org.x > 0){
			directionX = 1;
			if(nxtPoint.y-character.org.y > 0){
				directionY = 1;
			}else{
				directionY = -1;
			}
		}else{
			directionX = -1;
			if(nxtPoint.y-character.org.y > 0){
				directionY = 1;
			}else{
				directionY = -1;
			}
		}
		
	}

	public void render(GameContainer container, Graphics g) {
		g.setColor(Color.red);
		g.fillOval((float)character.org.x, (float)character.org.y, 10, 10);
		
	}

	@Override
	public double getX() {
		// TODO Auto-generated method stub
		return this.getCharacter().getOrigin().x;
	}

	@Override
	public double getY() {
		// TODO Auto-generated method stub
		return this.getCharacter().getOrigin().y;
	}

	@Override
	public int getRadius() {
		return 5;
	}
	
}
