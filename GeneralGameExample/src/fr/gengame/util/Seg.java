package fr.gengame.util;

import javax.print.attribute.standard.Destination;

import org.newdawn.slick.Graphics;

public class Seg {
	
	public Vec org, dir;
	
	public Seg(Vec pos, Vec dir)
	{
		this.org = pos;
		this.dir = dir;
	}
	
	public double length(){
		return dir.length();
	}
	
	public boolean isVertical(){
		if(dir.x == 0)return true;
		return false;
	}
	
	public boolean isHorizontal(){
		if(dir.y == 0)return true;
		return false;
	}
	
	
	
	public void setDir(Vec dir) {
		this.dir = dir;
	}

	//intersection de 2 segments verticaux / horizontaux
	public Vec interVorH(Seg s1, Seg s2){
		if(Vec.isColin(s1.dir, s2.dir))return null;
		
		if(s1.isHorizontal() && s2.isVertical())
			return new Vec(s2.org.x, s1.org.y);
		
		if(s1.isVertical() && s2.isHorizontal())
			return new Vec(s1.org.x, s2.org.y);
		
		return null;
	}
	
	public static Vec getIntersection(Seg segAM, Seg segBN)
	{
		double x1 = segAM.getOrigin().x, y1 = segAM.getOrigin().y, 
				x2 = segAM.getDest().x, y2 = segAM.getDest().y,  
				x3 = segBN.getOrigin().x,  y3 = segBN.getOrigin().y,  
				x4 = segBN.getDest().x, y4 = segBN.getDest().y;
		double d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
		if (d == 0) return null;
		double xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
		double yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;
		Vec p = new Vec(xi,yi);
		if (xi < Math.min(x1,x2) || xi > Math.max(x1,x2)) return null;
		if (xi < Math.min(x3,x4) || xi > Math.max(x3,x4)) return null;
		return p;
	}
	
	public static Vec getIntersectionLine(Seg segAM, Seg segBN)
	{
		double x1 = segAM.getOrigin().x, y1 = segAM.getOrigin().y, 
				x2 = segAM.getDest().x, y2 = segAM.getDest().y,  
				x3 = segBN.getOrigin().x,  y3 = segBN.getOrigin().y,  
				x4 = segBN.getDest().x, y4 = segBN.getDest().y;
		double d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
		if (d == 0) return null;
		double xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
		double yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;
		Vec p = new Vec(xi,yi);
		if(Vec.scal(Vec.sub(p, segAM.org), segAM.dir) < 0) return null;
		if(Vec.scal(Vec.sub(p, segBN.org), segBN.dir) < 0 || Vec.scal(Vec.sub(p, segBN.getDest()), segBN.dir) > 0) return null;
		return p;
	}
	
	public Vec proj(Vec M)
	{
		return Vec.sum(org, dir.proj(Vec.sub(M, org)));
	}

	public Vec getOrigin() {
		return org;
	}
	
	public Vec getDest()
	{
		return Vec.sum(org, dir);
	}
	
	public void draw(Graphics g)
	{
		g.drawLine((float)org.x, (float)org.y, (float)getDest().x, (float)getDest().y);
	}
}
