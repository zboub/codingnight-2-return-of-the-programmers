package fr.gengame.ennemies;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import fr.gengame.main.WorldGenGame;
import fr.gengame.menus.CreditsMenu;
import fr.gengame.util.Rectangle;
import fr.gengame.util.Seg;

public class Boss extends Ennemies implements Rectangle {
	private boolean init = true;
	private int hauteur;
	private int largeur;
	public Image skinFaible;
	public Image skinFort;
	public int vieFaible;

	public Boss(Seg character, int vie, int hauteur, int largeur) {
		super(character, vie);
		this.hauteur = hauteur;
		this.largeur = largeur;
		try {
			skinFaible = new Image("Images/boss3.png");
			skinFort = new Image("Images/boss4.png");
			this.skin = skinFort;
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update(int delta) {
		if (init) {
			this.setDirectionX(delta);
			this.setDirectionY(delta);
			init = false;
		}
	}

	public void render(Graphics g) {
		g.drawImage(skin, (float) (character.org.x),(float) (character.org.y));

	}
	
	public void die(){
		WorldGenGame.game.enterState(CreditsMenu.ID);
	}

	@Override
	public int getY() {
		// TODO Auto-generated method stub
		return (int) character.org.y;
	}

	@Override
	public int getX() {
		// TODO Auto-generated method stub
		return (int) character.org.x;
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return this.largeur;
	}

	@Override
	public int getHight() {
		// TODO Auto-generated method stub
		return this.hauteur;
	}
}
