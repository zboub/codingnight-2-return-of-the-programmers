package fr.gengame.ennemies;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import fr.gengame.character.Entity;
import fr.gengame.util.Circle;
import fr.gengame.util.Seg;

public class Ennemies extends Entity {
	private boolean init = true;

	public Ennemies(Seg character, int vie) {
		super(character, vie);
		// TODO Auto-generated constructor stub
	}

	public void update(int delta) {
		if (init) {
			this.setDirectionX(1);
			this.setDirectionY(1);
			init = false;
		}
		if (this.getCharacter().getOrigin().x > 800 - 32) {
			this.setDirectionX(-1);
		}
		if (this.getCharacter().getOrigin().y > 600 - 32) {
			this.setDirectionY(-1);
		}
		if (this.getCharacter().getOrigin().y < 32+60) {
			this.setDirectionY(1);
		}
		if (this.getCharacter().getOrigin().x < 32) {
			this.setDirectionX(1);
		}
		move(delta);
	}

	// ---------------------------------------------------
	// RENDER FUNCTION
	// ---------------------------------------------------

	public void render(Graphics g) {
		g.setColor(Color.red);
		g.fillOval((float) character.org.x-32, (float) character.org.y-32, 42, 42);
	}

}
