package fr.gengame.ennemies;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import fr.gengame.main.WorldGenGame;
import fr.gengame.util.Circle;
import fr.gengame.util.Collisions;
import fr.gengame.util.Seg;
import fr.gengame.util.Vec;

public class Sbires extends Ennemies implements Circle {

	private boolean dejaCol;
	int radius;
	int angle;
	int timeAngle;
	
	

	public Sbires(Seg character, int vie,int radius) {
		super(character, vie);
		this.radius = radius;
		WorldGenGame.ennemiesTab.add(this);
		WorldGenGame.ennemiesInit.add(this);
		try {
			this.skin = new Image("Images/enemy1.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.speed = new Vec(0.05,0.05);
	}

	@Override
	public double getX() {
		// TODO Auto-generated method stub
		return this.getCharacter().getOrigin().x;
	}

	@Override
	public double getY() {
		// TODO Auto-generated method stub
		return this.getCharacter().getOrigin().y;
	}

	@Override
	public int getRadius() {
		return radius;
	}

	public void nextCollision() {
		if (this.getDirectionX() == this.getDirectionY()) {
			this.setDirectionX(-1 * this.getDirectionX());
		} else {
			this.setDirectionY(-1 * this.getDirectionY());
		}
	}

	public void update(int delta) {
		super.update(delta);
		if (Collisions.collisionCircleRect(this, WorldGenGame.boss)) {
			if (dejaCol) {
				this.setDirectionX(-1 * this.getDirectionX());
				this.setDirectionY(-1 * this.getDirectionY());
				dejaCol = false;
			} else {
				this.nextCollision();
				dejaCol = true;
			}
		} else {
			dejaCol = false;
		}
	}
	
	public void render(Graphics g) {
		if(timeAngle >= 1){
		angle += 2;
		timeAngle=0;
		}
		else
		timeAngle ++;
	
		skin.setRotation(angle);
		g.drawImage(skin, (float) (character.org.x - 32),
				(float) (character.org.y - 32));
	}

}
